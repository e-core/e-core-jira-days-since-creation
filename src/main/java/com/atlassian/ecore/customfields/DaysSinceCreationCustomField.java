package com.atlassian.ecore.customfields;

import java.util.Map;

import javax.annotation.Nonnull;

import org.joda.time.DateTime;
import org.joda.time.Days;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;

public class DaysSinceCreationCustomField extends GenericTextCFType {

	public DaysSinceCreationCustomField(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager) {
		super(customFieldValuePersister, genericConfigManager);
	}

	public String getValueFromIssue(CustomField customField, Issue issue) {

		Integer days = null;

		String s = " day";

		DateTime created = new DateTime(issue.getCreated());
		DateTime today = new DateTime((DateTimeFormatter) ComponentAccessor.getComponent(DateTimeFormatter.class).forLoggedInUser().getZone());

		if ((issue.getResolutionObject() == null) && (issue.getCreated() != null)) {
			days = Integer.valueOf(Days.daysBetween(created, today).getDays());
		} else {
			DateTime resolutionDate = new DateTime(issue.getResolutionDate());
			days = Integer.valueOf(Days.daysBetween(created, resolutionDate).getDays());
		}

		if (days != null) {

			if (days != 1) {
				s = s.concat("s");
			}

			return String.valueOf(days).concat(s);
		}

		return null;

	}

	public Map<String, Object> getVelocityParameters(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem) {
		final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);

		return map;
	}

	@Override
	public int compare(@Nonnull String o1, @Nonnull String o2, FieldConfig fieldConfig) {

		o1 = o1.replaceAll("[^0-9]", "");
		o2 = o2.replaceAll("[^0-9]", "");

		int res = o1.compareTo(o2);

		if (res == 0 || !o1.equals(o2)) {
			return o1.hashCode() - o2.hashCode();
		}
		
		return res;

	}

}
